
(function () { // IIFE

	// Initialisation des données
	var index_phrase = 0,
		positionRat = 100,
		typed = document.getElementById('typed'),
		untyped = document.getElementById('untyped'),
		ouvreClavier = document.getElementById('ouvreClavier'),
		canvas = document.getElementById('terrain'),
		canvasWidth = canvas.parentElement.offsetWidth,
		ctx = canvas.getContext("2d"),
		sorcier = document.createElement("img"),
		bouledefeu = document.createElement("img"),
		rat = document.createElement("img"),
		phrases = [
			"a",
			"j",
			"f",
			"O ",
			"X",
			"b",
			"y",
			"G",
			"k",
			"S",
			"è",
			"ù",
		];

	canvas.height = 300;
	canvas.width = canvasWidth;
	sorcier.src = 'img/sorcier.png';
	bouledefeu.src = 'img/bouledefeu.png';
	rat.src = 'img/rat.png';
	var hauteurperso = 70,
		taillperso = 200,
		largeperso = 200;
	sorcier.onload = function () {
		ctx.drawImage(sorcier, 0, hauteurperso, 140, taillperso);
	};

	var drawrat = function () {
		ctx.drawImage(rat, 140 + (canvasWidth - 280) * positionRat / 100, hauteurperso, largeperso, taillperso);
	};
	rat.onload = drawrat;


	// Audio
	var flashbang = new Audio('sfx/flashbang.mp3'),
		grenade = new Audio('sfx/grenade.mp3'),
		pain = new Audio('sfx/pain.mp3');

	function initPhrase() {
		typed.textContent = '';
		untyped.textContent = phrases[index_phrase];
		positionRat = 100;
	}
	initPhrase();

	function clearRat() {
		ctx.clearRect(140 + (canvasWidth - 280) * positionRat / 100, hauteurperso, largeperso, taillperso);
	}

	document.addEventListener('keypress', logKey);

	document.onclick = function () {
		ouvreClavier.focus();
	};

	function logKey(e) {
		var key = e.key,
			first = untyped.textContent.charAt(0);

		clearRat();
		positionRat = positionRat - 1;
		drawrat();

		if (positionRat < 0) {
			pain.play();
			alert('Perdu !. Retour en arrière');
			index_phrase = index_phrase == 0 ? 0 : index_phrase - 1;
			clearRat();
			initPhrase();
		} else if (key == first) { // // Si la lettre tapée est premère lettre de untyped
			//  enlève première lettre de untyped et rajoute la à la fin de typed
			typed.textContent = typed.textContent + first;
			untyped.textContent = untyped.textContent.substr(1);
			if (untyped.textContent.trim().length == 0) {
				feu();
			}
		} else {
			// Sinon vide typed et ajoute le à typed
			untyped.textContent = typed.textContent + untyped.textContent;
			typed.textContent = '';
		}
	}

	function feu() {
		flashbang.play();
		var posBoule = 0,
			avance = function () {
				ctx.clearRect(140 + (canvasWidth - 280) * posBoule / 100, hauteurperso, largeperso, taillperso);
				posBoule = posBoule + 1;

				if (posBoule > positionRat) {
					window.clearInterval(intID);
					grenade.play();
					if (index_phrase == phrases.length) {
						alert('Gagné');
					} else {
						index_phrase++;
						initPhrase();
					}
				} else {
					ctx.drawImage(bouledefeu, 140 + (canvasWidth - 280) * posBoule / 100, hauteurperso, largeperso, taillperso);
				}
			};

		var intID = window.setInterval(avance, 10);
	}

})();